from decimal import Decimal


class ArithmeticEncoding:
    """
    ArithmeticEncoding is a class for building arithmetic encoding.
    """

    def __init__(self, frequency_table):
        self.probability_table = frequency_table

    def get_encoded_value(self, encoder):
        """
        After encoding the entire message, this method returns the single value that represents the entire message.
        """
        last_stage = list(encoder[-1].values())
        last_stage_values = []
        for sublist in last_stage:
            for element in sublist:
                last_stage_values.append(element)

        last_stage_min = min(last_stage_values)
        last_stage_max = max(last_stage_values)

        return (last_stage_min + last_stage_max) / 2

    def process_stage(self, probability_table, stage_min, stage_max):
        """
        Processing a stage in the encoding/decoding process.
        """
        stage_probs = {}
        stage_domain = stage_max - stage_min
        for term_idx in range(len(probability_table.items())):
            term = list(probability_table.keys())[term_idx]
            # term_prob = Decimal(probability_table[term])
            cum_prob = Decimal(probability_table[term]) * stage_domain + stage_min
            stage_probs[term] = [stage_min, cum_prob]
            stage_min = cum_prob
        return stage_probs

    def encode(self, msg, probability_table):
        """
        Encodes a message.
        """

        encoder = []

        stage_min = Decimal(0.0)
        stage_max = Decimal(1.0)

        for msg_term_idx in range(len(msg)):
            stage_probs = self.process_stage(probability_table, stage_min, stage_max)

            msg_term = msg[msg_term_idx]
            stage_min = stage_probs[msg_term][0]
            stage_max = stage_probs[msg_term][1]

            encoder.append(stage_probs)

        stage_probs = self.process_stage(probability_table, stage_min, stage_max)
        encoder.append(stage_probs)

        encoded_msg = self.get_encoded_value(encoder)

        return encoder, encoded_msg


def from_console():
    A = input('Введите словарь: ').split()
    P = input('Для него вероятности: ').split()

    # frequency_table = {"a": 0.2,
    #                    "b": 0.3,
    #                    "c": 0.1,
    #                    "d": 0.4}
    
    frequency_table = {}

    for i in range(len(A)):
        frequency_table[A[i]] = P[i]
        
    
    AE = ArithmeticEncoding(frequency_table)

    origin_message = input("Введите строку, которую хотите закодировать: ")

    encoder, encoded_msg = AE.encode(msg=origin_message,
                                     probability_table=AE.probability_table)

    print(f"Закодированное сообщение (точка на отрезке): {encoded_msg}")

    # print(f"Отрезки")
    # for i in encoder:
    #     print(i)
    # 
    print(f'Длина исходной строки: {len(origin_message)}')


def from_file():
    file_name = input("file name: ")
    with open(file_name, 'r') as file:
        text = file.readlines()
        A = text[0].split()
        P = text[1].split()

    frequency_table = {}

    for i in range(len(A)):
        frequency_table[A[i]] = P[i]
        
    AE = ArithmeticEncoding(frequency_table)

    origin_message = input("Введите строку, которую хотите закодировать: ")

    encoder, encoded_msg = AE.encode(msg=origin_message,
                                     probability_table=AE.probability_table)

    print(f"Закодированное сообщение (точка на отрезке): {encoded_msg}")

    # print(f"Отрезки")
    # for i in encoder:
    #     print(i)
    # 
    print(f'Длина исходной строки: {len(origin_message)}')


def main():
    while True:
        pick = input("1 read from file \n2 read from console\n")
        if pick == '1':
            from_file()
        elif pick == '2':
            from_console()


if __name__ == '__main__':
    main()
